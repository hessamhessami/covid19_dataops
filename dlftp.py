# -*-coding: utf-8 -*-
import os
import requests
url='https://www.data.gouv.fr/fr/datasets/r/6fadff46-9efd-4c53-942a-54aca783c30c'
response = requests.get(url)
with open(os.path.join(os.path.dirname(__file__), "incidence.csv"), 'wb') as f:
    f.write(response.content)        