import pandas as pd
import io
from io import StringIO
import json
import os
import requests, zipfile

class CONFIG:
    PROJECT_ID = "19208998"
    GITLAB_URL = "https://gitlab.com/api/v4/"
    API_KEY = "FbgzpTYDH6mnswx9FM9p"
    EXTRACT_DIR = "data/"

class NewsItem(object):
    def __init__(self, data):
        self.data = data
        self.id = int(data['id'])
    
    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        return isinstance(other, NewsItem) and self.id == other.id

    def __repr__(self):
        return "NewsItem:{0}".format(self.id)

def find_last_job_id(jobs_url):  
    r = requests.get(jobs_url, headers={"PRIVATE-TOKEN":CONFIG.API_KEY})
    jobs=json.loads(r.content)
    return(jobs[0]['id'])    

job_id = find_last_job_id((CONFIG.GITLAB_URL+"projects/{0}/jobs?scope[]=success").format(CONFIG.PROJECT_ID))
url = (CONFIG.GITLAB_URL+"projects/{0}/jobs/{1}/artifacts").format(CONFIG.PROJECT_ID, job_id)
r = requests.get(url, headers={"PRIVATE-TOKEN":CONFIG.API_KEY}, stream=True)
z = zipfile.ZipFile(io.BytesIO(r.content))
z.printdir()
inc=z.read("incidence.csv")
#txt=inc.decode("utf-8")
s=str(inc,'utf-8')
data=pd.read_csv(StringIO(s),sep=';')
 
#data = pd.read_csv('/Users/hihm_cb/MyProjects/Test_projects/incidence', sep=";")
data['jour']=pd.to_datetime(data['jour'])
data['dep'] = data['dep'].astype(str)
uptodate=max(pd.to_datetime(data['jour']))
lastdate=data['jour']==uptodate
#data['jour']=data['jour'].astype('datetime64[ns]')
data = data.loc[lastdate].drop(columns=['dep'])
data_series=data.sum(skipna = True)
#json=data_series.to_json(orient='columns')
data_series.to_json(os.path.abspath(os.path.dirname(__file__))+"/incidence.json")
 